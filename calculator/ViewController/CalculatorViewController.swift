//
//  CalculatorViewController.swift
//  calculator
//
//  Created by Jun Yee Sin on 7/26/15.
//  Copyright (c) 2015 Jun Yee Sin. All rights reserved.
//

import UIKit

class CalculatorViewController: UIViewController {

    var calculateLabel:UILabel!
    var total:Int = 0
    var number:String!
    var first_number : Int = 0
    var second_number : Int = 0
    var typingNumber : Bool = false
    var operation : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        calculateLabel = UILabel()
        calculateLabel.frame.origin = CGPoint(x: 20, y: 20)
        calculateLabel.frame.size = CGSize(width: 340, height: 100)
        calculateLabel.text = String(total)
        calculateLabel.backgroundColor = UIColor.redColor()
        calculateLabel.font = UIFont.boldSystemFontOfSize(40)
        self.view.addSubview(calculateLabel)
        
        setUpButton()
        
        self.view.backgroundColor = UIColor.blueColor()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpButton() {
        var button_ac: UIButton = UIButton()
        button_ac.frame.origin = CGPoint(x: 20, y: 570)
        button_ac.frame.size = CGSize(width: 100, height: 80)
        button_ac.setTitle("AC", forState: UIControlState.Normal)
        button_ac.backgroundColor = UIColor.blackColor()
        self.view.addSubview(button_ac)
        button_ac.addTarget(self, action: "clear:", forControlEvents: UIControlEvents.TouchUpInside)
        
        var button_equal: UIButton = UIButton()
        button_equal.frame.origin = CGPoint(x: 140, y: 570)
        button_equal.frame.size = CGSize(width: 220, height: 80)
        button_equal.setTitle("=", forState: UIControlState.Normal)
        button_equal.backgroundColor = UIColor.blackColor()
        self.view.addSubview(button_equal)
        button_equal.addTarget(self, action: "equals:", forControlEvents: UIControlEvents.TouchUpInside)
        
        var button_0: UIButton = UIButton()
        button_0.frame.origin = CGPoint(x: 20, y: 460)
        button_0.frame.size = CGSize(width: 100, height: 100)
        button_0.setTitle("0", forState: UIControlState.Normal)
        button_0.backgroundColor = UIColor.blackColor()
        self.view.addSubview(button_0)
        button_0.addTarget(self, action: "display:", forControlEvents: UIControlEvents.TouchUpInside)
        
        var button_add: UIButton = UIButton()
        button_add.frame.origin = CGPoint(x: 140, y: 460)
        button_add.frame.size = CGSize(width: 100, height: 100)
        button_add.setTitle("+", forState: UIControlState.Normal)
        button_add.backgroundColor = UIColor.blackColor()
        self.view.addSubview(button_add)
        button_add.addTarget(self, action: "calculation:", forControlEvents: UIControlEvents.TouchUpInside)
        
        var button_minus: UIButton = UIButton()
        button_minus.frame.origin = CGPoint(x: 260, y: 460)
        button_minus.frame.size = CGSize(width: 100, height: 100)
        button_minus.setTitle("-", forState: UIControlState.Normal)
        button_minus.backgroundColor = UIColor.blackColor()
        self.view.addSubview(button_minus)
        button_minus.addTarget(self, action: "calculation:", forControlEvents: UIControlEvents.TouchUpInside)
        
        var button_1: UIButton = UIButton()
        button_1.frame.origin = CGPoint(x: 20, y: 350)
        button_1.frame.size = CGSize(width: 100, height: 100)
        button_1.setTitle("1", forState: UIControlState.Normal)
        button_1.backgroundColor = UIColor.blackColor()
        self.view.addSubview(button_1)
        button_1.addTarget(self, action: "display:", forControlEvents: UIControlEvents.TouchUpInside)
        
        var button_2: UIButton = UIButton()
        button_2.frame.origin = CGPoint(x: 140, y: 350)
        button_2.frame.size = CGSize(width: 100, height: 100)
        button_2.setTitle("2", forState: UIControlState.Normal)
        button_2.backgroundColor = UIColor.blackColor()
        self.view.addSubview(button_2)
        button_2.addTarget(self, action: "display:", forControlEvents: UIControlEvents.TouchUpInside)
        
        var button_3: UIButton = UIButton()
        button_3.frame.origin = CGPoint(x: 260, y: 350)
        button_3.frame.size = CGSize(width: 100, height: 100)
        button_3.setTitle("3", forState: UIControlState.Normal)
        button_3.backgroundColor = UIColor.blackColor()
        self.view.addSubview(button_3)
        button_3.addTarget(self, action: "display:", forControlEvents: UIControlEvents.TouchUpInside)
        
        var button_4: UIButton = UIButton()
        button_4.frame.origin = CGPoint(x: 20, y: 240)
        button_4.frame.size = CGSize(width: 100, height: 100)
        button_4.setTitle("4", forState: UIControlState.Normal)
        button_4.backgroundColor = UIColor.blackColor()
        self.view.addSubview(button_4)
        button_4.addTarget(self, action: "display:", forControlEvents: UIControlEvents.TouchUpInside)
        
        var button_5: UIButton = UIButton()
        button_5.frame.origin = CGPoint(x: 140, y: 240)
        button_5.frame.size = CGSize(width: 100, height: 100)
        button_5.setTitle("5", forState: UIControlState.Normal)
        button_5.backgroundColor = UIColor.blackColor()
        self.view.addSubview(button_5)
        button_5.addTarget(self, action: "display:", forControlEvents: UIControlEvents.TouchUpInside)
        
        var button_6: UIButton = UIButton()
        button_6.frame.origin = CGPoint(x: 260, y: 240)
        button_6.frame.size = CGSize(width: 100, height: 100)
        button_6.setTitle("6", forState: UIControlState.Normal)
        button_6.backgroundColor = UIColor.blackColor()
        self.view.addSubview(button_6)
        button_6.addTarget(self, action: "display:", forControlEvents: UIControlEvents.TouchUpInside)
        
        var button_7: UIButton = UIButton()
        button_7.frame.origin = CGPoint(x: 20, y: 130)
        button_7.frame.size = CGSize(width: 100, height: 100)
        button_7.setTitle("7", forState: UIControlState.Normal)
        button_7.backgroundColor = UIColor.blackColor()
        self.view.addSubview(button_7)
        button_7.addTarget(self, action: "display:", forControlEvents: UIControlEvents.TouchUpInside)
        
        var button_8: UIButton = UIButton()
        button_8.frame.origin = CGPoint(x: 140, y: 130)
        button_8.frame.size = CGSize(width: 100, height: 100)
        button_8.setTitle("8", forState: UIControlState.Normal)
        button_8.backgroundColor = UIColor.blackColor()
        self.view.addSubview(button_8)
        button_8.addTarget(self, action: "display:", forControlEvents: UIControlEvents.TouchUpInside)
        
        var button_9: UIButton = UIButton()
        button_9.frame.origin = CGPoint(x: 260, y: 130)
        button_9.frame.size = CGSize(width: 100, height: 100)
        button_9.setTitle("9", forState: UIControlState.Normal)
        button_9.backgroundColor = UIColor.blackColor()
        self.view.addSubview(button_9)
        button_9.addTarget(self, action: "display:", forControlEvents: UIControlEvents.TouchUpInside)
    }
    
    
    func display(sender: AnyObject!) {
        number = sender.currentTitle
        if typingNumber {
            calculateLabel.text = calculateLabel.text! + number
        } else {
            calculateLabel.text = number
            typingNumber = true
        }
    }
    
    func calculation(sender: AnyObject!){
        typingNumber == false
        operation = sender.currentTitle
        first_number = calculateLabel.text!.toInt()!
        //calculateLabel.text = ""
        if (operation == "+"){
            total = first_number + second_number
        }
        if (operation == "-"){
            total = first_number - second_number
        }
    }
    
    func equals(sender: AnyObject!) {
        typingNumber == false
        second_number = calculateLabel.text!.toInt()!
        if (operation == "+"){
            total = first_number + second_number
        }
        if (operation == "-"){
            total = first_number - second_number
        }
        calculateLabel.text = String(total)
    }
    
    func clear(sender: AnyObject!) {
        first_number = 0
        second_number = 0
        total = 0
        calculateLabel.text = "0"
    }
    
    /*
    func addition(sender: AnyObject!) {
    typingNumber = false
    var calculate : String = calculateLabel.text!
    var num: Int = calculate.toInt()!
    total = total + num
    calculateLabel.text = String(total)
    }
    
    func subtraction(sender: AnyObject!) {
    typingNumber = false
    var calculate : String = calculateLabel.text!
    var num: Int = calculate.toInt()!
    total = total - num
    calculateLabel.text = String(total)
    }*/
    
    /*func calculate(sender: AnyObject!){
        operation = sender.currentTitle
        first_number = calculateLabel.text!.toInt()!
        second_number = number.toInt()!
        if (operation == "+"){
            total = first_number + second_number
        } else if (operation == "-"){
            total = first_number - second_number
        }
    }*/

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
